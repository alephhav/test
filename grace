## 欢迎大家的到来

因为环境特殊原因，防失联，请关注备份站

提示：全程**无需任何注册或者登录**，直接打开即可，若要下载下方连接，请复制地址到手机浏览中。

## 最新安卓APK下载
###### 1.长按手机复制下面任意一地址到手机浏览器下载安装
`https://mpimg.cn/down.php/d9dd3e11dcd049034db738d4fad653df.apk`

`https://od.lk/d/NjNfMzE3ODAzNTJf/%E4%BB%BB%E6%95%99%E5%B8%88.apk`

###### 2.复制下面地址到手机浏览器下载安装，搜索 **任教师** 关注即可 
`https://down11.wsyhn.com/app/Podcast-Go.apk`

**步骤**：安装进入软件之后，找到放大镜，输入  任教师  ，关注即可

## 苹果APP下载
在苹果商店下载**播客**
打开播客，点**现在收听**，点击**通过URL添加节目**，把下面这地址复制填入，**关注**即可

`https://www.podcasts.com/rss_feed/f6847d9c68de69e73a663b2d878948c3`



------------

## 1.在线站

- [https://bide153.com](https://bide153.com)

## 2.文字版

- [https://blog.bide153.com/](https://blog.bide153.com/)  密码153

## 3.加好友

<div align=left><img width = '150' height ='150' src ="https://txc.gtimg.com/data/394905/2022/0406/ed500e296c2db2a3d8353ded7c3762ff.jpeg"/></div>

## 4.添加新的聊天工具

**苹果手机，请使用中国区以外的ID登录苹果商店，下载skype安装即可**

请看具体图文操作，以安卓手机为例

**第一步，安装软件**

安卓手机长按复制以下面链接中的任意一个地址到手机浏览器

`https://mpimg.cn/down.php/24d3a70be07ea92648cf2d4efe8e3a77.apk`

`https://od.lk/d/NjNfMjU5MTAwMTdf/skype8.8.apk`



###### 第二步，注册新帐号

安装软件完成后，点击**新建帐号**，邮箱也可创建注册（需要登录邮箱查看验证码），

若手机无法注册，请用邮箱创建

###### 第三步，登录

登录后—找到->**联系人**

如下图

<div align=left><img  src ="https://txc.gtimg.com/data/394905/2022/0406/53359efb6ae59182230c4c01b550e2bd.png"/></div>

点击，找到一个图标上面有**+**号

<div align=left><img  src ="https://txc.gtimg.com/data/394905/2022/0406/60ae57a714a028d5da1162ecd95f2304.png"/></div>

###### 第四步：添加好友

搜索框中输入  **jemmy8288**

**请看图**

<div align=left><img  src ="https://txc.gtimg.com/data/394905/2022/0406/6e2f3354892999360db0dd24f0afb9fc.png"/></div>

添加即可成功



##### 最后的话

甘心支持我们，共同得天上的赏赐

<div align=left><img width = '150' height ='150' src ="https://txc.gtimg.com/data/394905/2022/0425/892673a03aeb4d775ccdf9c1226559ab.png"/></div>

